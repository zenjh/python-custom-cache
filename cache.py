import pickle
from functools import wraps
from logging import info, debug
from types import FunctionType

cache_list = {}


class Cache:
    data = {}

    def __init__(self, observable: FunctionType, file_name: str):
        self.observable = observable
        self.file_name = file_name or '%s.cache' % observable

    def save_to_file(self):
        with open(self.file_name, 'wb+') as f:
            str = pickle.dumps(self.data)
            f.seek(0)
            f.write(str)
            f.truncate()
            f.flush()
        debug('%s updates flushed to file' % (self.observable.__repr__()))

    def read_from_file(self):
        with open(self.file_name, 'rb') as f:
            self.data = pickle.load(f)
        debug('%s cache %d item loaded' % (self.observable.__repr__(), self.data.__len__()))

    @staticmethod
    def hash_key(*args, **kwargs):
        args_repr = list(map(lambda x: x.__dict__.__repr__(), args))
        return '_'.join(args_repr) + '-' + '_'.join(list(map(lambda x: x.__str__(), kwargs.values())))

    def update(self, hash_key, func_result=None):
        debug('Updating cached item result')
        self.observable.cache.data[hash_key] = result = func_result
        self.save_to_file()
        return result


def cached(func: FunctionType):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        # declaring 'self' ignores owner value from args - it's ok
        try:
            func.cache.read_from_file()
        except:
            info('No cache file')
        try:
            value = func.cache.data[func.cache.hash_key(*args, **kwargs)]
            debug('Returning cached result %s' % value)
            return value
        except KeyError:
            debug('Record not found in cache')
            result = func.cache.update(func.cache.hash_key(*args, **kwargs), func(*args, **kwargs))
            return result

    func.cache = Cache(func, file_name='%s.cache' % func.__name__)
    wrapper.cache = func.cache
    return wrapper
