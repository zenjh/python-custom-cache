# python-custom-cache

Annotation based caching function results

Usage:

Annotate method as cacheable:
    
    class SomeObject:
        @cached
        def get_some_data(self, foo, bar, **kwargs)
            pass

Fetch cache internal information:

    foo = SomeObject()
    foo.get_some_data.cache
    
